import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np

style.use('ggplot')

#ORIGINAL:

X = np.array([[1, 2],
              [1.5, 1.8],
              [5, 8],
              [8, 8],
              [1, 0.6],
              [9, 11]])


plt.scatter(X[:, 0],X[:, 1], s=50, linewidths = 5, zorder = 10)
plt.show()

colors = ["green","red","blue","yellow"]

class Mean_Shift:

    def __init__(self,radius=4):
        self.radius=radius

    def fit(self,data):
        centroids={}

        #Filling centroids with data
        for i in range(len(data)):
            centroids[i]=data[i]

        while True:
            new_centroids=[]
            for i in centroids:
                in_bandwidth=[] #will have all the featureset that comes under radius/bandwidth of centroid
                centroid=centroids[i] #initial centroid to start with
                for featureset in data:
                    #comparing if featureset comes under radius/bandwidth of centroid
                    if np.linalg.norm(featureset-centroid) < self.radius: #norm- magnitude of data point from origin
                        in_bandwidth.append(featureset)
                new_centroid=np.average(in_bandwidth,axis=0) #average of all featureset which comes under cluster with above centroid
                new_centroids.append(tuple(new_centroid))

            uniques=sorted(list(set(new_centroids))) # tracks the sorted list of all known centroids

            prev_centroids=dict(centroids) #storing current centroid before populating with new centroids found above
            centroids={}

            for i in range(len(uniques)):
                centroids[i]=np.array(uniques[i])

            optimized=True

            for i in centroids:
                #compare prev centroids with new centroids if both are equal it means that centroids are optimized no need to go further
                if not np.array_equal(centroids[i],prev_centroids[i]):
                    optimized=False
                if not optimized:
                    break

            if optimized:
                break
        self.centroids=centroids

    def predict(self):
        pass


clf = Mean_Shift()
clf.fit(X)

centroids = clf.centroids

plt.scatter(X[:,0], X[:,1], s=150)

for c in centroids:
    plt.scatter(centroids[c][0], centroids[c][1], color='k', marker='*', s=150)

plt.show()

